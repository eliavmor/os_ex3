#include "MapReduceClient.h"
#include "MapReduceFramework.h"
#include <string>
#include <dirent.h>
#include <iostream>


using namespace std;
#define LEGAL_ARGS_NUM 3

/**
 * A class that represents directory path.
 */
class DirPath :public k1Base ,public k2Base
{
public:
    /**
     * DirPath constructor.
     * @param path new DirPath.
     */
    DirPath(string path): _dirPath(path){}

    /**
     * DirPath copy constructor.
     * @param other DirPath object to copy.
     * @return A copy of the given DirPath object.
     */
    DirPath(const DirPath& other) : _dirPath(other.getName()) {}

    /**
     * DirPath distructor.
     */
    virtual ~DirPath() {}

    /**
     * Performs comparison based on the directory's path.
     * @param other k1Base object to perform comparison with.
     * @return true if this value's directory path is smaller (or equal) to the
     * other k1Base object (lexicographically), and false otherwise.
     */
    bool operator<(const k1Base &other) const override
    {
        return compareNames(((DirPath &)other));
    }

    /**
     * Performs comparison based on the directory's path.
     * @param other k2Base object to perform comparison with.
     * @return true if this value's directory path is smaller (or equal) to the
     * other k2Base object (lexicographically), and false otherwise.
     */
    bool operator<(const k2Base &other) const override
    {
        return compareNames(((DirPath &)other));
    }


    /**
     * Preforms comperation based on the driectory's path.
     * @param other - DirPath object to preform comparison with.
     * @return true if this value's direcptory path is smaller (or equal) to the
     * other DirPath object (lexicographically), otherwaise false.
     */
    bool compareNames(const DirPath &other) const
    {
        return this->_dirPath.compare(other.getName()) < 0;
    }

    /**
     * The function returns dir path.
     * @return dir path.
     */
    string getName() const { return _dirPath; }

private:
    /** directory path */
    string _dirPath;
};


class FileName : public v2Base, public k3Base
{
public:
    /**
     * Filename constructor.
     * @param fileName file name.
     */
    FileName(string fileName): _fileName(fileName){}

    /**
     * FileName distructor.
     */
    virtual ~FileName() {}

    /**
     * The function returns file name.
     * @return  file name.
     */
    string getName() const { return  _fileName; }

    /**
     * Performs comparison based on the file name.
     * @param other k3Base object to perform comparison with.
     * @return true if this value's filename is smaller (or equal) to the
     * other k3Base object (lexicographically), and false otherwise.
     */
    bool operator<(const k3Base &other) const override
    {
        FileName &other_filename = (FileName &) other;
        bool res = this->_fileName.compare(other_filename.getName()) < 0;
        return res;
    }


private:
    /** file name */
    string _fileName;
};


class MapReduce: public MapReduceBase
{
public:

    /**
     * MapReduce contructor.
     * @param identifier - An string that identify files.
     */
    MapReduce(string identifier) : _identifier(identifier) {}

    /**
     * MapReduce destructor.
     */
    virtual ~MapReduce(){}

    /**
     * The function gets a key and value and checks if the key is a driectory.
     * If it is it generates fileName objects from each driectory content and
     * adds it to MapReduceFramework.
     * @param key A key, represents a driectory path.
     * @param val A value to bind to given key.
     */
    void Map(const k1Base *const key, const v1Base *const val) const
    {
        DirPath *dir = (DirPath *) key;
        DIR* dirObj = opendir(dir->getName().c_str());
        struct dirent *ent;
        /* check if directory is a directory*/
        if(dirObj != NULL)
        {
            /* run over dirObj flies and create a FileName
               object for each file. */
            while ((ent = readdir (dirObj)) != NULL) {
                try {
                    Emit2(new DirPath(*dir), new FileName(ent->d_name));
                } catch (bad_alloc &e)
                {
                    cerr << e.what() << endl;
                }
            }
            closedir(dirObj);
        }
    }

    /**
     * The function gets a key and a vector of values and remove all values that
     * doesn't contains MapReduce indentifier.
     * @param key driectory name.
     * @param vals file name in directory.
     */
    void Reduce(const k2Base *const key, const V2_VEC &vals) const
    {
        for (auto val = vals.begin(); val != vals.end(); val++)
        {
            /* get file name */
            string filename = ((FileName*)(*val))->getName();

            /* if file name contains the identifier call Emit3 */
            if (filename.find(_identifier) != string::npos)
            {
                Emit3(new FileName(filename), nullptr);
            }
        }
    }

protected:
    /** A string that suppose to identify in files. */
    string _identifier;
};

int main (int argc, char *argv[])
{
    /* validate input size */
    if (argc < LEGAL_ARGS_NUM)
    {
        cerr << "Usage: <substring to search> <folders, separated by space>"
             << endl;
    }

    /* create MapReduce object with identifier 'argv[1]' */
    MapReduce *mapReduce = new MapReduce(argv[1]);
    IN_ITEMS_VEC itemsVec;

    /* iterate over the input and generate a vector of all the file names in
     * the directories */
    for (int i = 2; i < argc; ++i)
    {
        try{
            k1Base * k = (k1Base *)new DirPath(argv[i]);
            v1Base * v = nullptr;
            itemsVec.push_back(std::make_pair(k, v));
        } catch (bad_alloc &e) {
            cerr << e.what() << endl;
            return -1;
        }
    }

    OUT_ITEMS_VEC output = RunMapReduceFramework(*mapReduce, itemsVec, 10, true);

    /* print output */
    for (auto outIter = output.begin(); outIter != output.end(); outIter++)
    {
        cout << ((FileName*)(*outIter).first)->getName();
        /* prevent that the last print will have extra space.. */
        if (outIter+1 != output.end())
        {
            cout << " ";
        }
        delete (*outIter).first;
        delete (*outIter).second;
    }

    // free all item vec allocations.
    for (unsigned int j = 0; j < itemsVec.size(); ++j)
    {
        delete itemsVec[j].first;
        delete itemsVec[j].second;
    }
    delete mapReduce;
    return 0;
}
