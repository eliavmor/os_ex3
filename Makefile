HEADERS= MapReduceClient.h MapReduceFramework.h
CC = g++

all: MapReduceFramework Search

MapReduceFramework: MapReduceFramework.o
	ar rcs MapReduceFramework.a MapReduceFramework.o

Search: Search.o MapReduceClient.h
	$(CC) -g -pthread Search.o -L. MapReduceFramework.a -o Search

Test: test.o
	$(CC) -g -pthread test.o -L. MapReduceFramework.a -o Test

%.o: %.cpp $(HEADERS)
	$(CC) -pthread -Wall -Wvla -g -std=c++11 -c $<

valgrind: Test
	valgrind --tool=helgrind Test

clean:
	rm -vf *.o *.a *.tar Search Test

tar:
	tar -cvf ex3.tar MapReduceFramework.cpp Search.cpp Makefile README
