#include <stdlib.h>
#include "MapReduceFramework.h"
#include <semaphore.h>
#include <algorithm>
#include <fstream>
#include <zconf.h>
#include <sys/time.h>
#include <map>
#include <iostream>

using namespace std;

/*
 * Define constants for messages.
 */
#define CHUNK_SIZE 10 // Tasks chunk size.
#define LOG_FILE_NAME ".MapReduceFramework.log"
#define MICRO_TO_NANO 1000
#define SEC_TO_NANO 1000000000
#define CREATE_THREAD "pthread_create"
#define LOCK_THREAD "pthread_mutex_lock"
#define UNLOCK_THREAD "pthread_mutex_unlock"
#define DESTROY_THREAD "pthread_mutex_destroy"
#define INIT_THREAD "pthread_mutex_init"
#define TIME_ERR "gettimeofday"
#define JOIN_THREAD "pthread_join"
#define SEM_VALUE "sem_getvalue"
#define SEM_WAIT "sem_wait"
#define SEM_POST "sem_post"
#define SEM_INIT "sem_init"
#define MAP_REDUCE_FAILURE "MapReduceFramework Failure: "
#define FAILED " failed"

typedef std::pair<k2Base *, V2_VEC> SHUFFLE_PAIR;
typedef std::vector< std::pair<k2Base*, v2Base*> > MAP_CONTAINER;
typedef std::vector< std::pair<k3Base*, v3Base*> > REDUCE_CONTAINER;

/** Log file messages */
string START_LOG_MESSAGE = "RunMapReduceFramework started with ";
string THREADS_MESSAGE = " threads\n";
string MAP_THREAD_CREATED_MESSAGE = "Thread ExecMap created ";
string MAP_THREAD_TERMINATED_MESSAGE = "Thread ExecMap terminated ";
string shuffle_thread_CREATED_MESSAGE = "Thread Shuffle created ";
string shuffle_thread_TERMINATED_MESSAGE = "Thread Shuffle terminated ";
string REDUCE_THREAD_CREATED_MESSAGE = "Thread ExecReduce created ";
string REDUCE_THREAD_TERMINATED_MESSAGE = "Thread ExecReduce terminated ";
string MAP_SHUFFLE_TIME_MESSAGE = "Map and Shuffle took ";
string REDUCE_MESSAGE = "Reduce took ";
string FINISHED_MESSAGE = "RunMapReduceFramework finished\n";

/** Log file stream */
ofstream logStream;

/** A pointer of MapReduceBase object*/
MapReduceBase *mapReduceObject;

/**
 * A comperator for pairs of k2,v2.
 */
typedef struct k2Comparator
{
    bool operator()(k2Base * k1, k2Base * k2)
    {
        return *k1 < *k2;
    }
} k2Comparator;

// mutex for critical section checking global index
static pthread_mutex_t global_index_mutex;

// mutex for validating the creation of all map threads before beginning
static pthread_mutex_t create_threads_mutex;

// mutex for validating the creation of all reduce threads before beginning
static pthread_mutex_t reduce_threads_mutex;

// mutex for log file
static pthread_mutex_t log_file_mutex;

// mutex for shuffle output
static pthread_mutex_t shuffle_output_mutex;

// semaphore for shuffle thread
static sem_t shuffle_sem;

// final output after reduce
static OUT_ITEMS_VEC final_output;

// indicates if all the map threads finished executing
static bool map_threads_dead = false;

// indicates of the first available task.
static int global_index = 0;

// vector of all map containers
static std::map <unsigned long, MAP_CONTAINER> map_containers;

// vector for all map threads
static std::vector<pthread_t> map_threads;

// vector of all reduce containers
static std::map <unsigned long, REDUCE_CONTAINER> reduce_containers;

// vector for all reduce threads
static std::vector<pthread_t> reduce_threads;

// shuffle thread
static pthread_t shuffle_thread;

// map for shuffle - we'll move it to a vector after the shuffle is finished
static std::map<k2Base *, V2_VEC, k2Comparator> shuffle_helper;

// shuffle output - <key2 , List<v2> >
static std::vector<SHUFFLE_PAIR> shuffle_output;

// map containers mutexes - a mutex for each map container
static std::map<unsigned long , pthread_mutex_t> map_container_mutexes;

// input size - number of tasks.
static int input_items_size = 0;

// tasks vector.
static IN_ITEMS_VEC items_vec;

// temp vector for k2base *
static vector<k2Base *> k2Vector;

// temp vector for v2base *
static vector<v2Base *> v2Vector;
/**
 * The function checks if any errors occure in the given sys call result.
 * If errors do occure, print an error message to cerr and exit(1)
 * @param sysCallRes - syscall int result.
 * @param sysCallName - syscall name.
 */
static void syscall_check(int sysCallRes, string sysCallName) {
    if (sysCallRes) {
        if (sysCallRes == EBUSY && sysCallName == DESTROY_THREAD) { return; }
        cerr << MAP_REDUCE_FAILURE + sysCallName + FAILED << endl;
        exit(1);
    }
}

/*
 * Calculates the time it took for an operation to happen according to
 * the given parameters.
 * @param time_val_before timeval struct stating the time measured before the
 * operation.
 * @param time_val_after timeval struct stating the time measured after the
 * operation.
 * @return the total time the operation took in ns.
 */
static double calculate_time(timeval &time_val_before, timeval &time_val_after)
{
    double micro_start = time_val_before.tv_usec;
    double micro_end = time_val_after.tv_usec;
    double sec_start = time_val_before.tv_sec;
    double sec_end = time_val_after.tv_sec;
    double total = ((micro_end * MICRO_TO_NANO - micro_start * MICRO_TO_NANO)
                    + (sec_end * SEC_TO_NANO - sec_start * SEC_TO_NANO));

    return total;
}

/**
 * Writes to the cuurent date and time to the log file.
 */
static void writeTimeToLog()
{
    time_t current_time = time(0); // current time
    struct tm * current = localtime(&current_time);
    char buff[20];
    strftime (buff, sizeof(buff), "%d.%m.%Y %H:%M:%S", current);
    logStream <<  "[" << buff << "]\n" << flush;
}

/**
 * Supply an upper bound for map and reduce threads tasks.
 * @param index The index of the first task of the thread.
 * @return legal tasks bound index.
 */
static int get_upper_bound_items(int index)
{
    if(index + CHUNK_SIZE < input_items_size)
    {
        return index + CHUNK_SIZE;
    }
    return input_items_size;
}

/**
 * Supply an upper bound for shuffle tasks.
 * @param index The index of the first task of the thread.
 * @return legal tasks bound index.
 */
static int get_upper_bound_shuffle(int index)
{
    unsigned int sum = (unsigned int)index + CHUNK_SIZE;
    if(sum < shuffle_output.size())
    {
        return sum;
    }
    return (int)shuffle_output.size();
}

/**
 * Supply the index of the first avaliable task.
 * @return The index of the first avaliable task.
 */
static int getBaseIndex()
{
    int index = global_index;
    global_index += CHUNK_SIZE;
    return index;
}

/**
 * This function frees shuffle output vector.
 * @param autoDelete - if true free shuffle output othrewise won't.
 */
static void freeShuffleOutput(bool autoDelete)
{
    if (autoDelete)
    {
        for (unsigned int i = 0; i < k2Vector.size(); ++i)
        {
            delete k2Vector[i];
            k2Vector[i] = nullptr;
            delete v2Vector[i];
            v2Vector[i] = nullptr;
        }
    }
}

/**
 * The ExecMap function - it is tied to a single thread and continues
 * execution every time that thread awaknes.
 * @return nullptr.
 */
static void *execMap(void *)
{
    /**
     * lock thread (release when all threads created), initialize threads
     * container mutex and unlock the next thread when the lock realse.
     */
    syscall_check(pthread_mutex_lock(&create_threads_mutex), LOCK_THREAD);
    syscall_check(pthread_mutex_init(&map_container_mutexes[
            (unsigned long) pthread_self()], NULL), INIT_THREAD);
    map_containers[(unsigned long) pthread_self()];
    syscall_check(pthread_mutex_unlock(&create_threads_mutex), UNLOCK_THREAD);

    // get first available task index.
    syscall_check(pthread_mutex_lock(&global_index_mutex), LOCK_THREAD);
    int index = getBaseIndex();
    syscall_check(pthread_mutex_unlock(&global_index_mutex), UNLOCK_THREAD);

    bool tasks = true; // true as long as there are more tasks

    int upper_bound = get_upper_bound_items(index); // get upper bound for tasks
    while(tasks && index < input_items_size) // loop while there are more tasks
    {
        // run over tasks and execute map them.
        for(int i = index; i < upper_bound; ++i)
        {
            auto item = items_vec[i];
            mapReduceObject->Map(item.first, item.second);
        }

        // wake shuffle thread using sem_post.
        syscall_check(sem_post(&shuffle_sem), SEM_POST);

        // get new base index for executing more tasks.
        syscall_check(pthread_mutex_lock(&global_index_mutex), LOCK_THREAD);

        if(global_index < input_items_size) // check for more tasks
        {
            index = global_index;
            global_index = min(global_index + CHUNK_SIZE, input_items_size);
            upper_bound = get_upper_bound_items(index);
        }
        else
        {
            tasks = false; // there is no more tasks, thread will terminate.
        }
        syscall_check(pthread_mutex_unlock(&global_index_mutex), UNLOCK_THREAD);

    }
    // write termination of map thread to log file
    syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
    logStream << MAP_THREAD_TERMINATED_MESSAGE << flush;
    writeTimeToLog();
    syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);
    return nullptr;
}

/**
 * The ExecReduce function - it is tied to a single thread and continues
 * execution every time that thread awaknes.
 * @return nullptr.
 */
static void *execReduce(void *)
{
    /**
     * Lock all threads until all reduce threads are created and unlock threads
     * when the first thread is released
     **/
    syscall_check(pthread_mutex_lock(&reduce_threads_mutex), LOCK_THREAD);
    reduce_containers[(unsigned long) pthread_self()];
    syscall_check(pthread_mutex_unlock(&reduce_threads_mutex), UNLOCK_THREAD);

    // Get first available tasks index.
    syscall_check(pthread_mutex_lock(&global_index_mutex), LOCK_THREAD);
    int index = getBaseIndex();
    syscall_check(pthread_mutex_unlock(&global_index_mutex), UNLOCK_THREAD);

    bool tasks = true; // true as long as there are more tasks

    // Get upper bound for tasks that starts from 'index'
    syscall_check(pthread_mutex_lock(&shuffle_output_mutex), LOCK_THREAD);
    int upper_bound = get_upper_bound_shuffle(index);
    syscall_check(pthread_mutex_unlock(&shuffle_output_mutex), UNLOCK_THREAD);

    // while index is legal and has more tasks, execute reduce.
    while(tasks && (unsigned int)index < shuffle_output.size())
    {
        // run over tasks and execute reduce function.
        for(int i = index; i < upper_bound; ++i)
        {
            syscall_check(pthread_mutex_lock(&shuffle_output_mutex),
                          LOCK_THREAD);
            auto item = shuffle_output[i];
            syscall_check(pthread_mutex_unlock(&shuffle_output_mutex),
                          UNLOCK_THREAD);
            mapReduceObject->Reduce(item.first, item.second);
        }
        // failed to get the lock because another thread holds the lock
        syscall_check(pthread_mutex_lock(&global_index_mutex), LOCK_THREAD);

        // check for more tasks.
        if((unsigned int)global_index < shuffle_output.size())
        {
            index = global_index;
            global_index = min(global_index + CHUNK_SIZE,
                               (int)shuffle_output.size());
            upper_bound = get_upper_bound_shuffle(index);
        }
        else
        {
            tasks = false; // no more tasks, thread will terminate.
        }
        syscall_check(pthread_mutex_unlock(&global_index_mutex), UNLOCK_THREAD);

    }
    // write termination of reduce thread to log file
    syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
    logStream << REDUCE_THREAD_TERMINATED_MESSAGE << flush;
    writeTimeToLog();
    syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);
    return nullptr;
}

/**
 * This function executes when shuffle thread is running - shuffle all
 * map threads tasks. awakens when a map thread "submitted" a chunk of tasks
 * (sem_post).
 * @return nullptr
 */
static void *shuffle(void *)
{
    // lock thread until all map and shuffle threads were created and then
    // release the lock.
    syscall_check(pthread_mutex_lock(&create_threads_mutex), LOCK_THREAD);
    syscall_check(pthread_mutex_unlock(&create_threads_mutex), UNLOCK_THREAD);
    while(true)
    {
        // sem down - thread wait to tasks.
        syscall_check(sem_wait(&shuffle_sem), SEM_WAIT);

        // Thread woke by other map thred, look for a continer with tasks
        for(auto &map_container_pair : map_containers)
        {
            if(!map_container_pair.second.empty())
            {
                // found a continer with tasks! try to lock it and then shuffle
                // it tasks and clear it.
                syscall_check(pthread_mutex_lock
                (&map_container_mutexes[map_container_pair.first]),
                              LOCK_THREAD);

                MAP_CONTAINER container = map_container_pair.second;
                for(unsigned int j = 0; j < container.size(); ++j)
                {
                    shuffle_helper[container[j].first].push_back(
                            container[j].second);
                    k2Vector.push_back(container[j].first);
                    v2Vector.push_back(container[j].second);
                }
                map_containers[map_container_pair.first].clear();
                syscall_check(pthread_mutex_unlock
                        (&map_container_mutexes[map_container_pair
                        .first]), UNLOCK_THREAD);
            }
        }
        if(map_threads_dead) // if all threads are dead
        {
            int sem_val = -1;
            // get semaphore value
            syscall_check(sem_getvalue(&shuffle_sem, &sem_val), SEM_VALUE);
            // if semaphore value is 0 shuffle has no more tasks - exit loop.
            if (sem_val == 0)
            {
                break;
            }
        }
    }

    // add all map elements to a vector of shuffle output of <k2Base *, V2_VEC>
    for(auto& map_pair : shuffle_helper)
    {
        shuffle_output.push_back(map_pair);
    }

    // write termination of shuffle thread to log file
    syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
    logStream << shuffle_thread_TERMINATED_MESSAGE << flush;
    writeTimeToLog();
    syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);
    return nullptr;
}

/**
 * Initialize a vector of map threads in multiThreadLevel size.
 * @param multiThreadLevel - number of threads to create.
 **/
void initializeMapThreads(unsigned int multiThreadLevel)
{
    map_threads = vector<pthread_t>(multiThreadLevel);
    for(unsigned int i=0; i < multiThreadLevel; ++i)
    {
        syscall_check(pthread_create(&map_threads[i], NULL, execMap, NULL),
         CREATE_THREAD);
        // write creation of map thread to log file
        syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
        logStream << MAP_THREAD_CREATED_MESSAGE << flush;
        writeTimeToLog();
        syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);
    }
}

/**
 * Initiazlie a vector of reduce threads in multiThreadLevel size.
 * @param multiThreadLevel - number of threads to create.
 **/
void initializeReduceThreads(unsigned int multiThreadLevel)
{
    reduce_threads = vector<pthread_t>(multiThreadLevel);

    for(unsigned int i=0; i < multiThreadLevel; ++i)
    {
        syscall_check(pthread_create(
               &reduce_threads[i], NULL, execReduce, NULL), CREATE_THREAD);

        // write creation of reduce thread to log file
        syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
        logStream << REDUCE_THREAD_CREATED_MESSAGE << flush;
        writeTimeToLog();
        syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);
    }
}

/**
 * Initialize all containers and mutexes that perticipate in the library.
 */
static void initialize_all()
{
    // Initialize a vector of map threads.
    map_threads = vector<pthread_t>();
    // Initialize a vector of reduce threads.
    reduce_threads = vector<pthread_t>();
    // Initialize a map of map threads containers.
    map_container_mutexes = map<unsigned long, pthread_mutex_t>();
    // Initialize a map of reduce threads containers.
    reduce_containers = map <unsigned long, REDUCE_CONTAINER>();
    // Initialize a map for shuffling.
    shuffle_helper = map<k2Base *, V2_VEC, k2Comparator>();
    // Initialize shuffle output vector.
    shuffle_output = vector<SHUFFLE_PAIR>();
    // Initialize final output.
    final_output = vector<OUT_ITEM>();
    // Initialize indicator for all map threads finished
    map_threads_dead = false;
    // Initialize all mutex
    syscall_check(pthread_mutex_init(&create_threads_mutex, NULL), INIT_THREAD);
    syscall_check(pthread_mutex_init(&reduce_threads_mutex, NULL), INIT_THREAD);
    syscall_check(pthread_mutex_init(&log_file_mutex, NULL), INIT_THREAD);
    syscall_check(pthread_mutex_init(&shuffle_output_mutex, NULL), INIT_THREAD);
    syscall_check(pthread_mutex_init(&global_index_mutex, NULL), INIT_THREAD);
    // Initialize global index to 0
    syscall_check(pthread_mutex_lock(&global_index_mutex), LOCK_THREAD);
    global_index = 0;
    syscall_check(pthread_mutex_unlock(&global_index_mutex), UNLOCK_THREAD);
    input_items_size = 0;
}

/**
 * Clears all containers and destroys all mutexes that perticipate in the
 * library.
 */
static void clear_all()
{
    // Destroy mutexs
    syscall_check(pthread_mutex_destroy(&create_threads_mutex), DESTROY_THREAD);
    syscall_check(pthread_mutex_destroy(&reduce_threads_mutex), DESTROY_THREAD);
    syscall_check(pthread_mutex_destroy(&log_file_mutex), DESTROY_THREAD);
    syscall_check(pthread_mutex_destroy(&global_index_mutex), DESTROY_THREAD);
    syscall_check(pthread_mutex_destroy(&shuffle_output_mutex), DESTROY_THREAD);
    for(auto& mutex_pair : map_container_mutexes)
    {
        syscall_check(pthread_mutex_destroy(&mutex_pair.second),DESTROY_THREAD);
    }

    // Clear data structures
    map_threads.erase(map_threads.begin(), map_threads.end());
    map_container_mutexes.erase(map_container_mutexes.begin(),
                                map_container_mutexes.end());
    reduce_threads.erase(reduce_threads.begin(), reduce_threads.end());
    reduce_containers.erase(reduce_containers.begin(), reduce_containers.end());
    shuffle_helper.erase(shuffle_helper.begin(), shuffle_helper.end());
    shuffle_output.erase(shuffle_output.begin(), shuffle_output.end());
    k2Vector.erase(k2Vector.begin(), k2Vector.end());
    v2Vector.erase(v2Vector.begin(), v2Vector.end());

    map_threads.clear();
    map_container_mutexes.clear();
    reduce_threads.clear();
    reduce_containers.clear();
    shuffle_helper.clear();
    shuffle_output.clear();
    k2Vector.clear();
    v2Vector.clear();
}


/**
 * This function runs the framework facility MapReduce algorithem using
 * multi-threading.
 * @param mapReduce - A MapReduceBase object reference.
 * @param itemsVec - A vector of pairs k1base,v1base
 * @param multiThreadLevel - Number of threads to use.
 * @param autoDeleteV2K2 - An identifer for the library that indicates if it
 * should manage meemory free or user manage it.
 * @return - An vector of k3base,v3base
 */
OUT_ITEMS_VEC RunMapReduceFramework(MapReduceBase& mapReduce,
                                    IN_ITEMS_VEC& itemsVec,
                                    int multiThreadLevel, bool autoDeleteV2K2)
{
    initialize_all();
    struct timeval time_before_map_shuffle, time_after_map_shuffle;
    struct timeval time_before_reduce, time_after_reduce;
    syscall_check(gettimeofday(&time_before_map_shuffle, NULL), TIME_ERR);

    // open log file.
    logStream.open(LOG_FILE_NAME, std::fstream::in | std::fstream::out |
                                  std::fstream::app);
    if(!logStream)
    {
        cerr << MAP_REDUCE_FAILURE << "open failed" << endl;
    }
    if (!logStream.is_open())
    {
        logStream.open(LOG_FILE_NAME);
    }
    // write to log stream start message.
    syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
    logStream << START_LOG_MESSAGE << multiThreadLevel << THREADS_MESSAGE <<
                                                                          flush;
    syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);

    items_vec = itemsVec; // Initialize item_vec

    mapReduceObject = &mapReduce; // Initialize mapReduceObject pointer.
    input_items_size = (int)itemsVec.size(); // Initialize input_items_size

    // Start executing MAP-SHUFFLE part, lock all threads that creating.
    syscall_check(pthread_mutex_lock(&create_threads_mutex), LOCK_THREAD);
    // Initialize shuffle semaphore.
    syscall_check(sem_init(&shuffle_sem, 0, 0),SEM_INIT);
    initializeMapThreads(multiThreadLevel); // Create map threads.
    syscall_check(pthread_create(&shuffle_thread, NULL, shuffle, NULL),
                  CREATE_THREAD);

    // write creation of shuffle thread to log file
    syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
    logStream << shuffle_thread_CREATED_MESSAGE << flush;
    writeTimeToLog();
    syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);
    // begin execution of map threads
    syscall_check(pthread_mutex_unlock(&create_threads_mutex), UNLOCK_THREAD);
    // join all map threads
    for(unsigned int i=0; i < map_threads.size(); ++i)
    {
        syscall_check(pthread_join(map_threads[i], nullptr), JOIN_THREAD);
    }
    // if reached this point, it means all map threads are finished - change the
    // boolean variable and do shuffle_sem++ so that the semaphore will stop
    map_threads_dead = true;
    syscall_check(sem_post(&shuffle_sem), SEM_POST);
    syscall_check(pthread_join(shuffle_thread, nullptr), JOIN_THREAD);
    // measure time after MAP-SHUFFLE part.
    syscall_check(gettimeofday(&time_after_map_shuffle, NULL), TIME_ERR);
    // write time to log
    syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
    double time_map_shuffle = calculate_time(time_before_map_shuffle,
                                             time_after_map_shuffle);
    logStream << MAP_SHUFFLE_TIME_MESSAGE << time_map_shuffle << " ns\n";
    syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);
    // measure time before reduce part.
    syscall_check(gettimeofday(&time_before_reduce, NULL), TIME_ERR);

    // Start executing reduce part, initialize global index.
    syscall_check(pthread_mutex_lock(&global_index_mutex), LOCK_THREAD);
    global_index = 0;
    syscall_check(pthread_mutex_unlock(&global_index_mutex), UNLOCK_THREAD);

    syscall_check(pthread_mutex_lock(&reduce_threads_mutex), LOCK_THREAD);
    initializeReduceThreads(multiThreadLevel); // intialize reduce threads.
    syscall_check(pthread_mutex_unlock(&reduce_threads_mutex), UNLOCK_THREAD);
    // join all reduce threads
    for(unsigned int i=0; i < reduce_threads.size(); ++i)
    {
        syscall_check(pthread_join(reduce_threads[i], nullptr), JOIN_THREAD);
    }

    // merge all reduce containers into final output and sort
    for(auto& reduce_pair : reduce_containers )
    {
        for(unsigned int i=0; i < reduce_pair.second.size(); ++i)
        {
            final_output.push_back(reduce_pair.second[i]);
        }
    }
    // sort final output
    sort(final_output.begin(), final_output.end(), [](OUT_ITEM left,
    OUT_ITEM right) {return *(left.first) < *(right.first);});
    syscall_check(gettimeofday(&time_after_reduce, NULL), TIME_ERR);
    double time_reduce = calculate_time(time_before_reduce, time_after_reduce);
    syscall_check(pthread_mutex_lock(&log_file_mutex), LOCK_THREAD);
    logStream << REDUCE_MESSAGE << time_reduce << " ns\n" << FINISHED_MESSAGE
              << flush;
    logStream.close();
    syscall_check(pthread_mutex_unlock(&log_file_mutex), UNLOCK_THREAD);

    freeShuffleOutput(autoDeleteV2K2); // free shuffle output.

    clear_all();
    return final_output;
}

/**
 * This function moves elements from map part to reduce part,
 * The user should use this function inside MapReduceBase.map()
 * @param key - k2Base pointer
 * @param value v2Base pointer
 */
void Emit2 (k2Base* key, v2Base* value)
{
    syscall_check(pthread_mutex_lock(&map_container_mutexes[
                          (unsigned long) pthread_self()]),
                  LOCK_THREAD);
    map_containers[(unsigned long)pthread_self()].push_back(
            std::make_pair(key, value));
    syscall_check(pthread_mutex_unlock(&map_container_mutexes[
                          (unsigned long) pthread_self()]),
                  UNLOCK_THREAD);
}

/**
 * This function moves elements from reduce part to final output.
 * The user should use this function inside MapReduceBase.reduce()
 * @param key - k3Base pointer
 * @param value - v3Base pointer
 */
void Emit3 (k3Base* key, v3Base* value)
{
    reduce_containers[(unsigned long)pthread_self()].push_back(
            std::make_pair(key, value));
}
